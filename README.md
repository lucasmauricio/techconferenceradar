Tech Conference Radar
----------

**Tech Conference Radar** is a weekend project to drive [static site
generation](https://www.staticgen.com/) studies.

I chose the academic technology conferences as its main goal
to make it objective.

## Setting up the project with Statik

```
# Create the virtual environment, but explicitly specify to use Python 3
virtualenv -p python3 .

# Activate the virtual environment
source bin/activate

# Install Statik
pip install statik
```

Source: https://github.com/thanethomson/statik/wiki/Installation
